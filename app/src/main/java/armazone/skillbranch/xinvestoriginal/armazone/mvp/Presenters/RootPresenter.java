package armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters;



import android.content.Context;
import android.widget.Toast;

import javax.inject.Inject;

import armazone.skillbranch.xinvestoriginal.armazone.AzoneApp;
import armazone.skillbranch.xinvestoriginal.armazone.di.components.DaggerRootPresenterComponent;
import armazone.skillbranch.xinvestoriginal.armazone.di.components.RootPresenterComponent;
import armazone.skillbranch.xinvestoriginal.armazone.di.modules.RootPresenterModule;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Views.IRootView;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Models.AuthModel;


/**
 * Created by x-inv on 22.10.2016.
 */

public class RootPresenter implements IRootPresenter{

    private static RootPresenter instance = null;
    public static RootPresenter GetRootPresenter(){
        if (instance == null) instance = new RootPresenter();
        return instance;
    }

    @Inject
    AuthModel mAuthHelper;


    private IRootView iRootView;
    private boolean panelIsOpen;
    private String login;
    private String password;

    private RootPresenter(){
        //mAuthHelper = AuthModel.GetInstanse();

        this.iRootView = null;
        this.panelIsOpen = false;
        this.login = "";
        this.password = "";
        RootPresenterComponent component = createDaggerComponent();
        component.inject(this);
        mAuthHelper.rootPresenter = this;
    }

    private static boolean EMailIsValid(String source) {
        if (source == null) return false;
        String[] parts = source.split("@");
        if (parts.length != 2) return false;
        String address = parts[0];
        String domens = parts[1];
        if (address.length() == 0 || domens.length() == 0) return false;
        String[] subdomens = domens.split("\\.");
        if (subdomens.length < 2) return false;
        return subdomens[subdomens.length - 1].length() >= 2;
    }



    @Override
    public Object GetView() {
        return iRootView;
    }

    @Override
    public void DropPresenter() {
        iRootView = null;
        instance = null;
    }

    @Override
    public void takeView(Object view) {
        this.iRootView = (IRootView) view;
    }

    @Override
    public void initView() {
        iRootView.hideLoadProgress();
        if (panelIsOpen) {
            iRootView.showAuthPanel();
        }else{
            iRootView.hideAuthPanel();
        }
        iRootView.setAuthData(login,password);
    }

    @Override
    public void dropView() {
        this.iRootView = null;
    }


    @Override
    public void clickOnFb() {
        Context c = AzoneApp.getContext();
        Toast.makeText(c,"Заглушка для ФБ",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clickOnVk() {
        Context c = AzoneApp.getContext();
        Toast.makeText(c,"Заглушка для ВК",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clickOnTw() {
        Context c = AzoneApp.getContext();
        Toast.makeText(c,"Заглушка для ТВ",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clickOnGo(String login, String password) {
        this.login = login;
        this.password = password;
        if (this.panelIsOpen){
            boolean noError = GetToken(login,password);
            //Log.e("RootPresenter",String.valueOf(noError));
            if (noError){
                iRootView.showLoadProgress();
            }else{
                iRootView.showError("Что то пошло не так.");
            }
        }else{
            this.panelIsOpen = true;
            iRootView.showAuthPanel();
        }
    }



    @Override
    public void clickOnShowCatalog() {
        this.DropPresenter();
    }

    @Override
    public void onViewDestroy(String login, String password) {
        this.login = login;
        this.password = password;
    }


    private boolean GetToken(String mail, String pass){
        if (!EMailIsValid(mail)) return false;
        //Log.e("GetToken","GetToken");
        mAuthHelper.GetToken(mail,pass);
        return true;
    }

    public void onTokenObtained(){
        iRootView.hideLoadProgress();
        Context c = AzoneApp.getContext();
        Toast.makeText(c,"Токен получен",Toast.LENGTH_SHORT).show();
    }

    private RootPresenterComponent createDaggerComponent(){
        return DaggerRootPresenterComponent.
                builder().
                rootPresenterModule(new RootPresenterModule()).
                build();
    }
}
