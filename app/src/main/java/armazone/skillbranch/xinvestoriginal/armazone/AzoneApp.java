package armazone.skillbranch.xinvestoriginal.armazone;

import android.app.Application;
import android.content.Context;

/**
 * Created by x-inv on 03.11.2016.
 */

public class AzoneApp extends Application {

    public static Context getContext() {
        return sContext;
    }
    private static Context sContext;

    @Override
    public void onCreate(){
        super.onCreate();
        sContext = getApplicationContext();
    }
}
