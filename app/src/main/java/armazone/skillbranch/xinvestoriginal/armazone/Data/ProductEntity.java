package armazone.skillbranch.xinvestoriginal.armazone.Data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by x-inv on 28.10.2016.
 */

public class ProductEntity implements Parcelable {

    public String      id;
    public int      count;
    public float onePrice;
    public String    name;
    public String   descr;
    public String     url;

    protected ProductEntity(Parcel in) {
        count    = in.readInt();
        onePrice = in.readFloat();
        name     = in.readString();
        descr    = in.readString();
        url      = in.readString();
        id       = in.readString();
    }

    public ProductEntity(String id, String name, String descr, float onePrice){
        this.url = "";
        this.id = id;
        this.name = name;
        this.descr = descr;
        this.count = 0;
        this.onePrice = onePrice;
    }

    public ProductEntityLite toLite(){
        return new ProductEntityLite(id,count);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(count);
        dest.writeFloat(onePrice);
        dest.writeString(name);
        dest.writeString(descr);
        dest.writeString(url);
        dest.writeString(id);
    }

    public static final Creator<ProductEntity> CREATOR = new Creator<ProductEntity>() {
        @Override
        public ProductEntity createFromParcel(Parcel in) {
            return new ProductEntity(in);
        }

        @Override
        public ProductEntity[] newArray(int size) {
            return new ProductEntity[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


}
