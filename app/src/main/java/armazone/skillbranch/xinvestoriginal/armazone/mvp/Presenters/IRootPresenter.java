package armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters;


/**
 * Created by x-inv on 03.11.2016.
 */

public interface IRootPresenter extends IDefaultPresenter{
    void clickOnFb();
    void clickOnVk();
    void clickOnTw();
    void clickOnGo(String login, String password);
    void clickOnShowCatalog();
    void onViewDestroy(String login, String password);
}
