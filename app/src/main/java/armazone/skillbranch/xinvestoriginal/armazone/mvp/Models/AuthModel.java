package armazone.skillbranch.xinvestoriginal.armazone.mvp.Models;

import android.os.AsyncTask;

import armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters.RootPresenter;


/**
 * Created by x-inv on 22.10.2016.
 */

public class AuthModel {

    private static AuthModel instance = null;
    public static synchronized AuthModel GetInstanse(){
        if (instance == null){
            instance = new AuthModel();
        }
        return instance;
    }

    public RootPresenter rootPresenter;

    public AuthModel(){
        this.rootPresenter = null;
    }


    public boolean isAuth(){
        return false;
    }

    public void GetToken(String mail, String pass){


        //Log.e("GetToken","GetToken");
        new AsyncTask<Object,Object,Object>(){

            @Override
            protected Object doInBackground(Object... params) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object result) {
                super.onPostExecute(result);
                String token = (String) result;
                SetToken(token);
                final RootPresenter p = rootPresenter;
                if (p != null) rootPresenter.onTokenObtained();
            }

        }.execute();
    }

    private void SetToken(String token){

    }


}
