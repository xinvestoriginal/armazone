package armazone.skillbranch.xinvestoriginal.armazone.Helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by x-invest on 29.07.2016.
 */
public class AlertHelper {

    public static void ShowAlert(final Activity activity, String text){ShowAlert(activity,text,true);}
    public static void ShowAlert(final Activity activity, String text, final boolean exit){
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(text);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (exit) activity.finish();
            }
        });
        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertMenu = builder.create();
        alertMenu.show();
    }


}
