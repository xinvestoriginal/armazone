package armazone.skillbranch.xinvestoriginal.armazone.di.components;

import armazone.skillbranch.xinvestoriginal.armazone.di.modules.RootPresenterModule;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters.RootPresenter;
import dagger.Component;

/**
 * Created by x-inv on 04.11.2016.
 */

@Component (modules = RootPresenterModule.class)
public interface RootPresenterComponent {
    void inject(RootPresenter presenter);
}
