package armazone.skillbranch.xinvestoriginal.armazone.mvp.Views;

/**
 * Created by x-inv on 03.11.2016.
 */

public interface IRootView {
    void showError(String message);
    void showAuthPanel();
    void hideAuthPanel();
    void showLoadProgress();
    void hideLoadProgress();
    void setAuthData(String login, String password);
}
