package armazone.skillbranch.xinvestoriginal.armazone.mvp.Models;

import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntityLite;

/**
 * Created by x-inv on 05.11.2016.
 */

public interface ICartModel {
    void setProduct(ProductEntityLite product);
    int productCount(String id);
}
