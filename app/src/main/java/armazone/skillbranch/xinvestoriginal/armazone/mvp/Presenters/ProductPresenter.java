package armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters;

import android.support.annotation.Nullable;

import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntity;
import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntityLite;
import armazone.skillbranch.xinvestoriginal.armazone.Data.VirtualBase;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Models.CartModel;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Views.IProductView;

/**
 * Created by x-inv on 04.11.2016.
 */

public class ProductPresenter implements IProductPresenter{

    //private static ProductPresenter instance = null;
    //public static synchronized ProductPresenter GetInstance(){
    //    if (instance == null) instance = new ProductPresenter();
    //    return instance;
    //}

    private IProductView view;
    private ProductEntity visibleProduct;

    @Override
    public void takeView(Object view) {
        this.view = (IProductView) view;
    }

    public void takeView(Object view, String visibleProductId) {
        this.visibleProduct = VirtualBase.GetProduct(visibleProductId);
        this.takeView(view);
    }

    public float GetFullPrice(){
        return visibleProduct.onePrice * visibleProduct.count;
    }

    @Override
    public void initView() {
        view.initGui(visibleProduct.name,visibleProduct.descr,visibleProduct.url);
        view.setCount(visibleProduct.count);
        view.setFullPrice(visibleProduct.count * visibleProduct.onePrice);
    }

    @Override
    public void dropView() {
        view = null;
    }

    @Override
    public Object GetView() {
        return view;
    }

    @Override
    public void DropPresenter() {

    }

    @Override
    public void clickPlus() {
        visibleProduct.count++;
        view.setCount(visibleProduct.count);
        view.setFullPrice(GetFullPrice());
    }

    @Override
    public void clickMinus() {
        if (visibleProduct.count > 0){
            visibleProduct.count--;
            view.setCount(visibleProduct.count);
            view.setFullPrice(GetFullPrice());
        }
    }

    @Override
    @Nullable
    public ProductEntityLite GetSelectedProduct() {
        if (visibleProduct.count == 0){
            return null;
        }else{
            return new ProductEntityLite(visibleProduct.id,visibleProduct.count);
        }
    }

    @Override
    public ProductEntity getProductFromId(String id) {
        return VirtualBase.GetProduct(id);
    }

    @Override
    public int getCartCount(String id) {
        return CartModel.GetInstance().productCount(id);
    }
}
