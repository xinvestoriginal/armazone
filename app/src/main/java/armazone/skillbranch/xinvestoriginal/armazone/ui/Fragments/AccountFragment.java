package armazone.skillbranch.xinvestoriginal.armazone.ui.Fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import armazone.skillbranch.xinvestoriginal.armazone.Helpers.ImageHelper;
import armazone.skillbranch.xinvestoriginal.armazone.R;

/**
 * Created by x-inv on 28.10.2016.
 */

public  class AccountFragment extends Fragment  {

    private View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.content_account_screen, container, false);
        Bitmap roundAvatar = ImageHelper.GetRoundBitmap(getContext(),R.drawable.drawer_avatar);
        ImageView ivHeaderPhoto = (ImageView)rootView.findViewById(R.id.ivAccountPhoto);
        ivHeaderPhoto.setImageBitmap(roundAvatar);
        return rootView;
    }



    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }



}
