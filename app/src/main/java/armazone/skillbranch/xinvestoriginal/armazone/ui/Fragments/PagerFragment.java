package armazone.skillbranch.xinvestoriginal.armazone.ui.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.matthewtamlin.sliding_intro_screen_library.indicators.DotIndicator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntityLite;
import armazone.skillbranch.xinvestoriginal.armazone.Helpers.AlertHelper;
import armazone.skillbranch.xinvestoriginal.armazone.R;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters.CatalogPresenter;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters.ICatalogPresenter;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters.ProductPresenter;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Views.ICatalogView;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Views.IProductView;
import armazone.skillbranch.xinvestoriginal.armazone.ui.Adapters.SectionsPagerAdapter;

/**
 * Created by x-inv on 28.10.2016.
 */

public  class PagerFragment extends Fragment implements View.OnClickListener, ICatalogView, ViewPager.OnPageChangeListener {

    private ViewPager mViewPager;
    private ICatalogPresenter iCatalogPresenter;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_pager, container, false);
        rootView.findViewById(R.id.bMainAddToCart).setOnClickListener(this);
        InitPager(0);
        iCatalogPresenter = CatalogPresenter.GetInstance();
        return rootView;
    }

    private void InitPager(int currentPagerPos){
        mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity().getSupportFragmentManager());
        mViewPager = (ViewPager)rootView.findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        ProductFragment.SetIndex(currentPagerPos);
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setCurrentItem(currentPagerPos);
    }

    @Override
    public void onStart(){
        super.onStart();
    }


    private ProductEntityLite GetProduct(int pos){
        ProductFragment page = (ProductFragment)getActivity().getSupportFragmentManager().
                findFragmentByTag("android:switcher:" + R.id.container + ":" + pos);
        if (page != null){
            return page.GetSelectedProduct();
        }else{
            return null;
        }
    }





    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.bMainAddToCart:
                ProductEntityLite product = GetProduct(mViewPager.getCurrentItem());
                //Log.e(">>>",String.valueOf(product));
                if (product != null){
                    iCatalogPresenter.addCardClick(product);
                    Toast.makeText(getContext(),"товар в количестве " + String.valueOf(product.count) + " штук добавлен в карзину", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getContext(),"Количество товара не может быть == 0", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


    @Override
    public void showMessage(String msg) {
        Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void swapLoadState(boolean loadIsVisible) {

    }

    @Override
    public void swapAuthPanelState(boolean panelIsVisible) {

    }

    @Override
    public void updateCartCount(String count, String price) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int pos) {
        DotIndicator dotIndicator = (DotIndicator)rootView.findViewById(R.id.dots);
        ProductFragment.SetIndex(pos);
        int itemIndex = Math.round(dotIndicator.getNumberOfItems() *
                pos / mSectionsPagerAdapter.getCount());
        dotIndicator.setSelectedItem(itemIndex,true);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
