package armazone.skillbranch.xinvestoriginal.armazone.di.modules;

import armazone.skillbranch.xinvestoriginal.armazone.mvp.Models.AuthModel;
import dagger.Module;
import dagger.Provides;

/**
 * Created by x-inv on 04.11.2016.
 */


@Module
public class RootPresenterModule {
    @Provides
    AuthModel provideAuthModel(){
        return new AuthModel();
    }
}
