package armazone.skillbranch.xinvestoriginal.armazone.ui.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.HashMap;
import java.util.Map;

import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntity;
import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntityLite;
import armazone.skillbranch.xinvestoriginal.armazone.Data.VirtualBase;
import armazone.skillbranch.xinvestoriginal.armazone.ui.Fragments.ProductFragment;

/**
 * Created by x-inv on 28.10.2016.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ProductFragment.newInstance(String.valueOf(position));
    }

    @Override
    public int getCount() {
        return 3;
    }
}
