package armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters;

/**
 * Created by x-inv on 03.11.2016.
 */

public interface IDefaultPresenter {
      void takeView(Object view);
      void initView();
      void dropView();
      Object GetView();
      void DropPresenter();
}
