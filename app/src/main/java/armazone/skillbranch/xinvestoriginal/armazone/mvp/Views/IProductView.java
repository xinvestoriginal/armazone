package armazone.skillbranch.xinvestoriginal.armazone.mvp.Views;

import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntityLite;

/**
 * Created by x-inv on 04.11.2016.
 */

public interface IProductView {
    void setCount(int count);
    void setFullPrice(float price);
    void initGui(String name, String description, String imageUrl);
    ProductEntityLite GetSelectedProduct();
}
