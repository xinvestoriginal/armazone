package armazone.skillbranch.xinvestoriginal.armazone.ui.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntity;
import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntityLite;
import armazone.skillbranch.xinvestoriginal.armazone.R;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters.ProductPresenter;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Views.IProductView;

/**
 * Created by x-inv on 28.10.2016.
 */

public  class ProductFragment extends Fragment implements View.OnClickListener, IProductView{

    private static final String PRODUCT_ID = "idProduct";
    private static ArrayList<Integer> indexes = new ArrayList<>();

    public  static void SetIndex(int index){
        if (indexes.size() == 0){
            indexes.add(index);
        }else{
            int lastIndex = indexes.get(indexes.size() - 1);
            if (lastIndex != index) indexes.add(index);
        }
    }

    @Nullable
    public static Integer GetBackIndex(){
        if (indexes.size() < 2) return null;
        int res = indexes.get(indexes.size() - 2);
        indexes.remove(indexes.size() - 1);
        return res;
    }

    private ProductPresenter productPresenter;
    private String productId;
    private View rootView = null;

    public static ProductFragment newInstance(String productId) {
        ProductFragment fragment = new ProductFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PRODUCT_ID,productId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_product, container, false);
        rootView.findViewById(R.id.ivProductMinus).setOnClickListener(this);
        rootView.findViewById(R.id.ivProductPlus).setOnClickListener(this);
        productId = getArguments().getString(PRODUCT_ID);
        SetPresenter();
        return rootView;
    }

    @Override
    public void onStart(){
        super.onStart();
    }


    private void SetPresenter(){
        productPresenter = new ProductPresenter();
        productPresenter.takeView(this,productId);
        productPresenter.initView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivProductMinus:
                productPresenter.clickMinus();
                break;
            case R.id.ivProductPlus:
                productPresenter.clickPlus();
                break;
        }
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    @Override
    public void setCount(int count) {
        TextView tvProductCountValue = (TextView)rootView.findViewById(R.id.tvProductCountValue);
        tvProductCountValue.setText(String.valueOf(count));
    }

    @Override
    public void setFullPrice(float price) {
        TextView tvProductPriceValue = (TextView)rootView.findViewById(R.id.tvProductPriceValue);
        tvProductPriceValue.setText(String.format("%.2f",price) + "p");
    }

    @Override
    public void initGui(String name, String description, String imageUrl) {
        TextView textView = (TextView)rootView.findViewById(R.id.tvProductName);
        textView.setText(name);

        TextView tvProductDescription = (TextView)rootView.findViewById(R.id.tvProductDescription);
        tvProductDescription.setText(description);

        ImageView ivProductPictures = (ImageView)rootView.findViewById(R.id.ivProductPictures);
        Picasso.with(getContext()).load(imageUrl).placeholder(R.drawable.shop_icon)
                .error(R.drawable.shop_icon).into(ivProductPictures);
    }

    @Override
    public ProductEntityLite GetSelectedProduct() {
        return this.productPresenter != null ? this.productPresenter.GetSelectedProduct() : null;
    }
}
