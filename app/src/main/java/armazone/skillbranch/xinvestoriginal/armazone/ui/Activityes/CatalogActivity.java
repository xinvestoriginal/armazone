package armazone.skillbranch.xinvestoriginal.armazone.ui.Activityes;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.util.List;

import armazone.skillbranch.xinvestoriginal.armazone.Helpers.AlertHelper;
import armazone.skillbranch.xinvestoriginal.armazone.Helpers.ImageHelper;
import armazone.skillbranch.xinvestoriginal.armazone.R;
import armazone.skillbranch.xinvestoriginal.armazone.ui.Fragments.AccountFragment;
import armazone.skillbranch.xinvestoriginal.armazone.ui.Fragments.PagerFragment;

public class CatalogActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{


    private static final String MAIN_FRAME = "mainFrame";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = InitToolBar();
        NavigationView nv = InitDrawer(toolbar);
        Bitmap roundAvatar = ImageHelper.GetRoundBitmap(this,R.drawable.drawer_avatar);
        ImageView ivHeaderPhoto = (ImageView)nv.getHeaderView(0).findViewById(R.id.ivHeaderPhoto);
        ivHeaderPhoto.setImageBitmap(roundAvatar);

        FragmentManager fm = getSupportFragmentManager();
        Fragment oldFragment = fm.findFragmentByTag(MAIN_FRAME);
        if (oldFragment == null){
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fPager, new PagerFragment(),MAIN_FRAME);
            ft.commit();
        }
    }

    private void ShowAccount(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fPager, new AccountFragment(),MAIN_FRAME);
        ft.commit();
    }

    private Toolbar InitToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("");
        return toolbar;
    }

    private NavigationView InitDrawer(Toolbar toolbar){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        return navigationView;
    }

    private Fragment GetLastFragment(){
        List<Fragment> list = getSupportFragmentManager().getFragments();
        Fragment res = null;
        for (int i = list.size() - 1; i >= 0 && res == null; i--){
            Fragment tempFragment = list.get(i);
            if (tempFragment != null && MAIN_FRAME.equals(tempFragment.getTag())){
                res = tempFragment;
            }
        }
        return res;
    }

    private int GetScreenCount(){
        List<Fragment> list = getSupportFragmentManager().getFragments();
        int res = 0;
        for (int i = list.size() - 1; i >= 0; i--){
            Fragment tempFragment = list.get(i);
            if (tempFragment != null && MAIN_FRAME.equals(tempFragment.getTag())) res++;
        }
        return res;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else{

            if (GetScreenCount() > 1){
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction trans = fm.beginTransaction();
                trans.remove(GetLastFragment());
                trans.commit();
                fm.popBackStack();
            }else{
                AlertHelper.ShowAlert(this,getString(R.string.realy_exit_question));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_cart) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_account) {
            ShowAccount();
        } else if (id == R.id.nav_orders) {
            //iCatalogPresenter.goOrdersClick();
        } else if (id == R.id.nav_favorite) {
            //iCatalogPresenter.goFavoriteClick();
        } else if (id == R.id.nav_catalog) {
            //iCatalogPresenter.goCatalogClick();
        } else if (id == R.id.nav_notification) {
            //iCatalogPresenter.goNotificationClick();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
