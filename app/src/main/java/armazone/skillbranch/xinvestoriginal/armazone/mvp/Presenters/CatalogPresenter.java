package armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters;

import android.content.Context;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import armazone.skillbranch.xinvestoriginal.armazone.AzoneApp;
import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntityLite;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Models.CartModel;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Views.ICatalogView;

/**
 * Created by x-inv on 03.11.2016.
 */

public class CatalogPresenter implements ICatalogPresenter {
    private static CatalogPresenter instance = null;
    public static synchronized CatalogPresenter GetInstance(){
        if (instance == null) instance = new CatalogPresenter();
        return instance;
    }


    private ICatalogView view;

    @Override
    public void takeView(Object view) {
        this.view = (ICatalogView) view;
    }

    @Override
    public void initView() {

    }

    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public Object GetView() {
        return view;
    }

    @Override
    public void DropPresenter() {
        dropView();
        instance = null;
    }

    @Override
    public void addCardClick(ProductEntityLite product) {
        CartModel.GetInstance().setProduct(product);
    }

    @Override
    public void goCabinetMenuClick() {
        Context c = AzoneApp.getContext();
        Toast.makeText(c,"Заглушка для Кабинета",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goOrdersClick() {
        Context c = AzoneApp.getContext();
        Toast.makeText(c,"Заглушка для Заказа",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goFavoriteClick() {
        Context c = AzoneApp.getContext();
        Toast.makeText(c,"Заглушка для Избранного",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goCatalogClick() {
        Context c = AzoneApp.getContext();
        Toast.makeText(c,"Заглушка для Каталога заказов",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goNotificationClick() {
        Context c = AzoneApp.getContext();
        Toast.makeText(c,"Заглушка для Уведомлений",Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean userIsAuth() {
        return false;
    }
}
