package armazone.skillbranch.xinvestoriginal.armazone.mvp.Models;

import java.util.HashMap;
import java.util.Map;

import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntityLite;

/**
 * Created by x-inv on 05.11.2016.
 */

public class CartModel implements ICartModel{
    private static CartModel instance = null;
    public static synchronized CartModel GetInstance(){
        if (instance == null) instance = new CartModel();
        return instance;
    }
    private Map<String, ProductEntityLite> products;
    private CartModel(){
        products = new HashMap<>();
    }

    @Override
    public void setProduct(ProductEntityLite product) {
        products.put(product.id,product);
    }

    @Override
    public int productCount(String id) {
        return products.containsKey(id) ? products.get(id).count : 0;
    }
}
