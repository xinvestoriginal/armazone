package armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters;

import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntity;
import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntityLite;

/**
 * Created by x-inv on 04.11.2016.
 */

public interface IProductPresenter extends IDefaultPresenter{
    void clickPlus();
    void clickMinus();
    ProductEntityLite GetSelectedProduct();
    ProductEntity getProductFromId(String id);
    int getCartCount(String id);
}
