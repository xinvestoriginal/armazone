package armazone.skillbranch.xinvestoriginal.armazone.mvp.Views;

/**
 * Created by x-inv on 04.11.2016.
 */

public interface ICatalogView {
    void showMessage(String msg);
    void showError(String msg);
    void swapLoadState(boolean loadIsVisible);
    void swapAuthPanelState(boolean panelIsVisible);
    void updateCartCount(String count, String price);
}
