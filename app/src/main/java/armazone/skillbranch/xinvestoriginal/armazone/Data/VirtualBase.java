package armazone.skillbranch.xinvestoriginal.armazone.Data;

/**
 * Created by x-inv on 28.10.2016.
 */

public class VirtualBase {

    public static ProductEntity GetProduct(String id){
        ProductEntity res = new ProductEntity(id,"","",0);

        if ("0".equals(id)){
            res.onePrice = 2707.34f;
            res.name = "ASUS M5A78L-M LX3";
            res.descr = "материнская плата форм-фактора microATX\n" +
                    "сокет AM3+\n" +
                    "чипсет AMD 760G\n" +
                    "2 слота DDR3 DIMM, 1066-1866 МГц\n" +
                    "видеоадаптер ATI Radeon HD3000";
            res.url = "https://mdata.yandex.net/i?path=b0218231503_img_id7558528614682280379.jpg&size=5";
        }
        if ("1".equals(id)){
            res.onePrice = 3505.14f;
            res.name = "ASUS B85M-G";
            res.descr = "материнская плата форм-фактора microATX\n" +
                    "сокет LGA1150\n" +
                    "чипсет Intel B85\n" +
                    "4 слота DDR3 DIMM, 1066-1600 МГц";
            res.url = "https://mdata.yandex.net/i?path=b0611151352_img_id387235534296406784.jpg&size=5";
        }
        if ("2".equals(id)){
            res.onePrice = 11696.71f;
            res.name = "MSI Z170A GAMING M5";
            res.descr = "материнская плата форм-фактора ATX\n" +
                    "сокет LGA1151\n" +
                    "чипсет Intel Z170\n" +
                    "4 слота DDR4 DIMM, 2133-3600 МГц\n" +
                    "поддержка SLI/CrossFireX\n" +
                    "разъемы SATA: 6 Гбит/с - 6";
            res.url = "https://mdata.yandex.net/i?path=b0817124536_img_id4868295046102863486.jpeg&size=5";
        }
        return res;
    }
}
