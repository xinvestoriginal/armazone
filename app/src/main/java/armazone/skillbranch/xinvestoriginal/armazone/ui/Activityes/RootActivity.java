package armazone.skillbranch.xinvestoriginal.armazone.ui.Activityes;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import armazone.skillbranch.xinvestoriginal.armazone.Helpers.TypefaceHelper;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters.RootPresenter;
import armazone.skillbranch.xinvestoriginal.armazone.R;
import armazone.skillbranch.xinvestoriginal.armazone.mvp.Views.IRootView;

public class RootActivity extends AppCompatActivity implements View.OnClickListener, IRootView{

    RootPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        TextView tvAuthAppName = (TextView)findViewById(R.id.tvAuthAppName);
        TypefaceHelper.SetTypeface(this,tvAuthAppName);

        findViewById(R.id.bAuthGo).setOnClickListener(this);
        findViewById(R.id.bAuthShowCatalog).setOnClickListener(this);

        InitSocialButtons();

        presenter = RootPresenter.GetRootPresenter();
        presenter.takeView(this);
        presenter.initView();

    }

    private void InitSocialButtons(){
        Animation aniFB = AnimationUtils.loadAnimation(this, R.anim.sots);
        View ivAuthGoFB = findViewById(R.id.ivAuthGoFB);
        ivAuthGoFB.setOnClickListener(this);
        aniFB.setStartOffset(600L);
        ivAuthGoFB.setAnimation(aniFB);

        Animation aniTV = AnimationUtils.loadAnimation(this, R.anim.sots);
        View ivAuthGoTV = findViewById(R.id.ivAuthGoTV);
        ivAuthGoTV.setOnClickListener(this);
        aniTV.setStartOffset(300L);
        ivAuthGoTV.setAnimation(aniTV);

        Animation aniVC = AnimationUtils.loadAnimation(this, R.anim.sots);
        View ivAuthGoVC = findViewById(R.id.ivAuthGoVC);
        ivAuthGoVC.setOnClickListener(this);
        aniVC.setStartOffset(0);
        ivAuthGoVC.setAnimation(aniVC);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed(){
        finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bAuthGo:
                String vEmail = ((EditText)findViewById(R.id.etAuthMail)).getText().toString();
                String vPass = ((EditText)findViewById(R.id.etAuthPassword)).getText().toString();
                presenter.clickOnGo(vEmail,vPass);
                break;
            case R.id.ivAuthGoFB:
                presenter.clickOnFb();
                break;
            case R.id.ivAuthGoTV:
                presenter.clickOnTw();
                break;
            case R.id.ivAuthGoVC:
                presenter.clickOnVk();
                break;
            case R.id.bAuthShowCatalog:
                presenter.clickOnShowCatalog();
                startActivity(new Intent(this,CatalogActivity.class));
                finish();
                break;

        }
    }

    @Override
    public void showError(String message) {
        View view = findViewById(R.id.llAuthMainContainer);
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
        //Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAuthPanel() {
        findViewById(R.id.llAuthPanelContainer).setVisibility(View.VISIBLE);
        findViewById(R.id.llAuthShowButtonContainer).setVisibility(View.GONE);
    }

    @Override
    public void hideAuthPanel() {
        findViewById(R.id.llAuthPanelContainer).setVisibility(View.GONE);
        findViewById(R.id.llAuthShowButtonContainer).setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadProgress() {
        findViewById(R.id.llAuthMainContainer).setVisibility(View.GONE);
        findViewById(R.id.pbAuthLoad).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadProgress() {
        findViewById(R.id.llAuthMainContainer).setVisibility(View.VISIBLE);
        findViewById(R.id.pbAuthLoad).setVisibility(View.GONE);
    }

    @Override
    public void setAuthData(String login, String password) {
        ((EditText)findViewById(R.id.etAuthMail)).setText(login);
        ((EditText)findViewById(R.id.etAuthPassword)).setText(password);
    }

    @Override
    public void onDestroy(){
        String vEmail = ((EditText)findViewById(R.id.etAuthMail)).getText().toString();
        String vPass = ((EditText)findViewById(R.id.etAuthPassword)).getText().toString();
        presenter.onViewDestroy(vEmail,vPass);
        super.onDestroy();
    }
}
