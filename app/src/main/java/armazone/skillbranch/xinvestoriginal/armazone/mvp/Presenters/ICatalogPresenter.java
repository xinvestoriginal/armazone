package armazone.skillbranch.xinvestoriginal.armazone.mvp.Presenters;

import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntity;
import armazone.skillbranch.xinvestoriginal.armazone.Data.ProductEntityLite;

/**
 * Created by x-inv on 04.11.2016.
 */

public interface ICatalogPresenter extends IDefaultPresenter{
    void addCardClick(ProductEntityLite productEntity);
    void goCabinetMenuClick();
    void goOrdersClick();
    void goFavoriteClick();
    void goCatalogClick();
    void goNotificationClick();
    boolean userIsAuth();
}
