package armazone.skillbranch.xinvestoriginal.armazone.Helpers;

import android.content.Context;
import android.graphics.*;
import android.media.ExifInterface;
import android.net.Uri;

import java.io.IOException;

/**
 * Created by x-invest on 29.08.2016.
 */

public class ImageHelper {

    private static final int HEADER_SIZE = 64;

    public static Bitmap GetRoundBitmap(Context context, int resource){
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),resource);
        return GetHeaderBitmap(bitmap,false);
    }

    public static Bitmap GetRoundBitmap(Bitmap source){
        return GetHeaderBitmap(source,false);
    }

    public static Bitmap GetHeaderBitmap(Bitmap source, boolean resize){
        if (!resize) return GetRCBitmap(source);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(source, HEADER_SIZE, HEADER_SIZE, false);
        return GetRCBitmap(scaledBitmap);
    }

    private static Bitmap GetRCBitmap(Bitmap bitmap) {
        int width     = bitmap.getWidth();
        int height    = bitmap.getHeight();
        Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color     = 0xff424242;
        final Paint paint   = new Paint();
        final Rect  rect    = new Rect(0, 0, width, height);
        final RectF rectF   = new RectF(rect);
        final float roundPx = width < height ? width / 2 : height / 2;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }



    private static Bitmap RotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static Bitmap RotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return RotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return RotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return RotateImage(img, 270);
            default:
                return img;
        }
    }
}
